import Axios from 'axios';

const axios = Axios.create({
    baseURL: 'https://warm-plains-06070.herokuapp.com/',
    timeout: 10000,
});

export default axios;
