module.exports = {

    primary: '#b8de6f',
    secondary: '#bbbbbb',
    tertiary: '#ff847c',

    background: '#f6f6f6',
    danger: '#ec524b',

    black: '#000000',
    white: '#FFFFFF',
    grey: 'grey',
    lightGrey: '#EFEFEF',
    green: '#61b15a',

};
