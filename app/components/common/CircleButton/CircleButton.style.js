import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    button: {
        width: RFValue(50),
        height: RFValue(50),
        backgroundColor: colors.secondary,
        borderRadius: 1000,
        borderWidth: 2,
        borderColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
    },

    icon: {
        height: RFValue(25),
        width: RFValue(25),
        color: colors.white,
    },

});
