import React from 'react';

import { TouchableOpacity, Text } from 'react-native';

import styles from './CircleButton.style';
import { colors } from 'FillTheFridge/app/constants';

import CheckIcon from 'FillTheFridge/app/assets/svg/check_icon.svg';
import PenIcon from 'FillTheFridge/app/assets/svg/pen_icon.svg';
import PenClearIcon from 'FillTheFridge/app/assets/svg/pen_clear_icon.svg';
import BackIcon from 'FillTheFridge/app/assets/svg/arrow_back.svg';
import SearchIcon from 'FillTheFridge/app/assets/svg/search_icon.svg';
import AddIcon from 'FillTheFridge/app/assets/svg/add_product.svg';
import CrossIcon from 'FillTheFridge/app/assets/svg/cross_icon.svg';

const CircleButton = (props) => {
    const { buttonStyle, icon, onPress } = props;

    return (
        <TouchableOpacity
            style={[styles.button, buttonStyle]}
            onPress={onPress}
        >

            {icon === 'check' && <CheckIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}
            {icon === 'modify' && <PenIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}
            {icon === 'clear' && <PenClearIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}
            {icon === 'back' && <BackIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}
            {icon === 'search' && <SearchIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}
            {icon === 'addProduct' && <AddIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}
            {icon === 'cross' && <CrossIcon
                height={styles.icon.height}
                width={styles.icon.height}
                fill={styles.icon.color}
            />}

        </TouchableOpacity>
    )
}

export default CircleButton;
