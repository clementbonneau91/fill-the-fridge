import React from 'react';

import { SafeAreaView, StatusBar } from 'react-native';

import styles from './Screen.style';
import { colors } from '../../../constants';

const Screen = (props) => {
    const { children } = props;

    return (
        <SafeAreaView style={styles.container}>

            <StatusBar barStyle={'light-content'}/>

            {children}

        </SafeAreaView>
    )
};

export default Screen;
