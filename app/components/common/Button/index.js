import React from 'react';

import { TouchableOpacity, Text } from 'react-native';

import styles from './Button.style';
import { colors } from '../../../constants';

const Button = (props) => {
    const { buttonStyle, title, onPress, disabled } = props;

    return (
        <TouchableOpacity
            style={[styles.button, buttonStyle, disabled ? {backgroundColor: colors.grey} : {}]}
            disabled={disabled}
            onPress={onPress}
        >

            <Text style={styles.text}>{title}</Text>

        </TouchableOpacity>
    )
}

export default Button;
