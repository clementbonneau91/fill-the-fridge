import { StyleSheet } from 'react-native';
import { colors } from '../../../constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    button: {
        width: '95%',
        height: 50,
        marginVertical: 10,
        backgroundColor: colors.secondary,
        borderRadius: RFValue(5),
        borderWidth: 2,
        borderColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.white,
        fontSize: RFValue(12),
        fontWeight: 'bold',
    },

});
