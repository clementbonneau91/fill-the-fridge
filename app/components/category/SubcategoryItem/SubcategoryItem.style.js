import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    sectionContainer: {
        height: 50,
        backgroundColor: colors.primary,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sectionTitle: {
        color: colors.white,
        fontSize: RFValue(15),
        fontWeight: 'bold',
    },

});
