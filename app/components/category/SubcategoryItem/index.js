import React from 'react';

import { View, Text } from 'react-native';

import styles from './SubcategoryItem.style';

const SubcategoryItem = (props) => {
    const { title } = props;
    
    return (
        <View style={styles.sectionContainer}>

            <Text style={styles.sectionTitle}>{title}</Text>

        </View>
    )
};

export default SubcategoryItem;
