import React from 'react';

import { TouchableOpacity, ImageBackground, View, Text } from 'react-native';

import styles from './CategoryItem.style';
import CheckIcon from 'FillTheFridge/app/assets/svg/check_circle.svg';

const CategoryItem = (props) => {
    const { style, title, image, onPress, selected } = props;

    return (
        <TouchableOpacity style={[styles.container, style]} onPress={onPress}>

            <ImageBackground source={{uri: image}} style={styles.image}>

                <View style={styles.opacityView}>

                    <Text style={styles.text}>{title}</Text>

                    {selected &&
                        <CheckIcon
                            style={styles.selectedIcon}
                            width={styles.icon.width}
                            height={styles.icon.width}
                            fill={styles.icon.color}
                        />
                    }

                </View>

            </ImageBackground>

        </TouchableOpacity>
    )
};

export default CategoryItem;
