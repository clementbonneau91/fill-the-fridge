import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    container: {
        height: 120,
        marginVertical: 5,
        backgroundColor: colors.tertiary,
    },

    opacityView: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.2)',
        justifyContent: 'center',
        alignItems: 'center',
    },

    image: {
        resizeMode: 'contain',
        height: '100%',
    },

    text: {
        textAlign: 'center',
        width: '90%',
        color: colors.white,
        fontSize: RFValue(18),
        fontWeight: 'bold',
    },

    selectedIcon: {
        position: 'absolute',
        top: 10,
        right: 10,
    },
    icon: {
        width: RFValue(25),
        color: colors.green,
    },

});
