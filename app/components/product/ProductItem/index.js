import React from 'react';

import { View, TouchableOpacity, Text } from 'react-native';

import styles from './ProductItem.style';
import { colors } from 'FillTheFridge/app/constants';
import AddIcon from 'FillTheFridge/app/assets/svg/add_circle.svg';
import RemoveIcon from 'FillTheFridge/app/assets/svg/remove_circle.svg';
import CheckIcon from 'FillTheFridge/app/assets/svg/check_circle.svg';

const ProductItem = (props) => {
    const { product, onPress, onLongPress, removing, onList, validatedList, checked, checkProduct, uncheckProduct } = props;

    return (
        <TouchableOpacity
            style={[styles.itemContainer, {backgroundColor: validatedList && checked ? colors.lightGrey : colors.white}]}
            activeOpacity={1}
            onLongPress={onLongPress}
        >

            <Text style={[styles.itemTitle, {textDecorationLine: validatedList && checked ? 'line-through' : 'none'}]}>{product.title}</Text>

            <TouchableOpacity onPress={onPress} disabled={onList}>

                {onList &&
                    <CheckIcon
                        width={styles.checkIcon.width}
                        height={styles.checkIcon.width}
                        fill={styles.checkIcon.color}
                    />
                }
                {!onList && !removing &&
                    <AddIcon
                        width={styles.addIcon.width}
                        height={styles.addIcon.width}
                        fill={styles.addIcon.color}
                    />
                }
                {!validatedList && !onList && removing &&
                    <RemoveIcon
                        width={styles.removeIcon.width}
                        height={styles.removeIcon.width}
                        fill={styles.removeIcon.color}
                    />
                }
                {validatedList &&
                    <TouchableOpacity
                        style={styles.circleButton}
                        onPress={() => checked ? uncheckProduct() : checkProduct()}
                    >
                        {checked && <View style={styles.checkedCircle}></View>}
                    </TouchableOpacity>
                }

            </TouchableOpacity>

        </TouchableOpacity>
    )
};

export default ProductItem;
