import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    itemContainer: {
        height: 50,
        marginVertical: 1,
        paddingHorizontal: RFValue(10),
        backgroundColor: colors.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemTitle: {
        color: colors.black,
        fontSize: RFValue(12),
    },

    checkIcon: {
        color: colors.green,
        width: RFValue(20),
    },
    addIcon: {
        color: colors.tertiary,
        width: RFValue(20),
    },
    removeIcon: {
        color: colors.danger,
        width: RFValue(20),
    },
    circleButton: {
        width: RFValue(20),
        height: RFValue(20),
        borderRadius: 1000,
        borderWidth: 1,
        borderColor: colors.black,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkedCircle: {
        width: '80%',
        height: '80%',
        backgroundColor: colors.green,
        borderRadius: 1000,
    },

});
