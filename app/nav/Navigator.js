import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { MyList, HomeLibrary, AddProduct } from '../screens';
import { Screen } from '../components';

import { colors } from '../constants';

const TopTab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

const topTabOptions = {
    style: {
        backgroundColor: colors.tertiary,
    },
    indicatorStyle: {
        backgroundColor: colors.primary,
    },
    activeTintColor: colors.white,
};

const TopTabNavigator = () => (
    <TopTab.Navigator tabBarOptions={topTabOptions}>
        <TopTab.Screen
            name="Ma liste"
            component={MyList}
        />
        <TopTab.Screen
            name="Ajouter"
            component={HomeLibrary}
        />
    </TopTab.Navigator>
);

const StackNavigator = () => (
    <Stack.Navigator
        mode="modal"
        screenOptions={{
            headerShown: false,
        }}
    >
        <Stack.Screen
            name="StartScreen"
            component={TopTabNavigator}
        />

        {/* Modals */}
        <Stack.Screen
            name="AddProduct"
            component={AddProduct}
        />
    </Stack.Navigator>
);

const Navigator = () => (
        <NavigationContainer>
            <Screen>
                <StackNavigator/>
            </Screen>
        </NavigationContainer>
);

export default Navigator;
