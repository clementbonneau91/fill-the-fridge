import axios from '../utils/axios';

export const fetchCategories = () => axios.get('/categories')
    .then((res) => res.data.results);
