import axios from '../utils/axios';

export const fetchSubcategories = () => axios.get('/subcategories')
    .then((res) => res.data.results);
