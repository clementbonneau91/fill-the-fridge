import MyList from './MyList';
import HomeLibrary from './Library/Home';
import AddProduct from './Modals/AddProduct';

module.exports = {
    MyList,
    HomeLibrary,
    AddProduct,
};
