import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { View, TouchableOpacity, ScrollView, Text, TextInput, FlatList, ImageBackground, Keyboard, Alert } from 'react-native';
import { Button, CategoryItem } from 'FillTheFridge/app/components';

import styles from './AddProduct.style';
import CancelIcon from 'FillTheFridge/app/assets/svg/cancel.svg';

import { addNewProduct } from 'FillTheFridge/app/store/products/actions';

const AddProduct = () => {
    const dispatch = useDispatch();
    const { goBack } = useNavigation();

    const { categories, subcategories } = useSelector(state => state.productsReducer);

    const [selectedCategory, setSelectedCategory] = useState(null);
    const [selectedSubcategory, setSelectedSubcategory] = useState(null);
    const [productName, setProductName] = useState('');

    const renderCategoryItem = ({ item }) => {
        return (
            <CategoryItem
                style={styles.categoryItem}
                title={item.title}
                image={item.image}
                onPress={() => {
                    setSelectedCategory(item.id);
                    setSelectedSubcategory(null);
                }}
                selected={item.id === selectedCategory}
            />
        );
    };

    const renderSubcategoryItem = ({ item }) => {
        if (item.category.id === selectedCategory) {
            return (
                <CategoryItem
                    style={styles.categoryItem}
                    title={item.title}
                    image={item.image}
                    onPress={() => setSelectedSubcategory(item.id)}
                    selected={item.id === selectedSubcategory}
                />
            );
        };
    };

    const onSubmit = async () => {
        await dispatch(addNewProduct({
            subcategoryId: selectedSubcategory,
            title: productName.trim(),
        }));
        goBack();
    };

    return (
        <View style={styles.container} onPress={() => Keyboard.dismiss()}>

            <ScrollView>

                <View style={styles.header}>
                    <Text style={styles.title}>Ajouter un produit</Text>
                    <TouchableOpacity style={styles.cancel} onPress={() => goBack()}>
                        <CancelIcon width={styles.icon.width} height={styles.icon.width} fill={styles.icon.color}/>
                    </TouchableOpacity>
                </View>

                <Text style={styles.subtitle}>Nom du produit</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={text => setProductName(text)}
                    value={productName}
                />

                <Text style={styles.subtitle}>Catégorie</Text>
                <FlatList
                    style={styles.flatlist}
                    data={categories}
                    renderItem={renderCategoryItem}
                    keyExtractor={(item, index) => item.title}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />

                {selectedCategory && <Text style={styles.subtitle}>Sous catégorie</Text>}
                {selectedCategory && <FlatList
                    style={styles.flatlist}
                    data={subcategories}
                    renderItem={renderSubcategoryItem}
                    keyExtractor={(item, index) => item.title}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />}

                <Button
                    buttonStyle={styles.button}
                    title={'Valider'}
                    onPress={() => onSubmit()}
                    disabled={!selectedCategory || !selectedSubcategory || productName.length === 0}
                />

            </ScrollView>

        </View>
    )
};

export default AddProduct;
