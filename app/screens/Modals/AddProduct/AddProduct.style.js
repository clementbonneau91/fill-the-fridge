import { StyleSheet, Dimensions } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

const win = Dimensions.get('window');

export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.background,
    },

    header: {
        marginBottom: RFValue(20),
        backgroundColor: colors.primary,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        textAlign: 'center',
        marginVertical: RFValue(15),
        marginLeft: RFValue(10),
        color: colors.white,
        fontSize: RFValue(15),
        fontWeight: 'bold',
    },
    cancel: {
        position: 'absolute',
        right: RFValue(10),
    },
    icon: {
        width: RFValue(25),
        color: colors.white,
    },
    subtitle: {
        marginLeft: RFValue(10),
        color: colors.tertiary,
        fontSize: RFValue(13),
    },

    input: {
        height: 50,
        marginHorizontal: RFValue(10),
        marginTop: RFValue(20),
        marginBottom: RFValue(30),
        paddingHorizontal: 10,
        color: colors.black,
        fontSize: RFValue(12),
        backgroundColor: colors.white,
        borderWidth: 1.5,
        borderColor: colors.tertiary,
        borderRadius: RFValue(4),
    },

    flatlist: {
        alignSelf: 'center',
        marginVertical: RFValue(14),
        paddingHorizontal: RFValue(6),
        width: win.width,
    },
    categoryItem: {
        width: win.width / 2.5,
        height: win.width / 2.5,
        marginHorizontal: RFValue(5),
        backgroundColor: colors.tertiary,
    },

    button: {
        alignSelf: 'center',
        backgroundColor: colors.green,
    },

});
