import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { View, ScrollView, Text, Alert } from 'react-native';
import { SubcategoryItem, ProductItem, CircleButton } from 'FillTheFridge/app/components';

import styles from './MyList.style';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

import { removeProductToList, clearMyList, validateList, checkProduct, uncheckProduct, uncheckAllProducts } from 'FillTheFridge/app/store/list/actions';

const MyList = () => {
    const { myList, validatedList, checkList } = useSelector(state => state.listReducer);

    const dispatch = useDispatch();

    const removeToList = (subcategory, product) => {
        dispatch(uncheckProduct(product));
        dispatch(removeProductToList(subcategory, product));
    };

    const isChecked = (product) => {
        let checked = false;

        checkList.forEach((item, i) => {
            if (item.title === product.title) {
                checked = true;
            }
        });

        return checked;
    };

    const clearList = () => {
        Alert.alert(
            "Vider la liste",
            "Es-tu sûr.e de vouloir retirer tous les produits de ta liste ?",
            [
              {
                text: "Annuler",
                style: "cancel"
              },
              {
                  text: "Vider la liste",
                  onPress: () => dispatch(clearMyList())
              }
            ],
            { cancelable: false }
          );
    };

    if (myList.length === 0) {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Rendez-vous dans la rubrique AJOUTER pour constituer votre liste de courses</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>

            <ScrollView showsVerticalScrollIndicator={false}>

                {myList.map((item, index) => (
                    <View key={index} style={myList.length === index + 1 ? {marginBottom: RFValue(70)} : {}}>

                        <SubcategoryItem title={item.subcategory}/>

                        {item.products.map((product, key) => (
                            <ProductItem
                                key={key}
                                product={product}
                                onPress={() => removeToList(item.subcategory, product)}
                                removing
                                validatedList={validatedList}
                                checked={isChecked(product)}
                                checkProduct={() => dispatch(checkProduct(product))}
                                uncheckProduct={() => dispatch(uncheckProduct(product))}
                            />
                        ))}

                    </View>
                ))}

            </ScrollView>

            {myList.length > 0 &&
                <View style={styles.footerContainer}>

                    <CircleButton
                        icon={validatedList ? 'clear' : 'cross'}
                        buttonStyle={{backgroundColor: validatedList ? colors.grey : colors.danger}}
                        onPress={() => validatedList ? dispatch(uncheckAllProducts()) : clearList()}
                    />

                    <CircleButton
                        icon={validatedList ? 'modify' : 'check'}
                        buttonStyle={{backgroundColor: validatedList ? colors.tertiary : colors.primary}}
                        onPress={() => validatedList ? dispatch(validateList(false)) : dispatch(validateList(true))}
                    />

                </View>
            }

        </View>
    )
}

export default MyList;
