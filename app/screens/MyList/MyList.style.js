import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.background,
    },

    text: {
        alignSelf: 'center',
        textAlign: 'center',
        width: '80%',
        marginTop: RFValue(100),
        color: colors.tertiary,
        fontSize: RFValue(12),
    },

    footerContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        paddingHorizontal: RFValue(10),
        paddingVertical: RFValue(10),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

});
