import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { View, KeyboardAvoidingView, Image, FlatList, TextInput, Alert } from 'react-native';
import { SubcategoryItem, ProductItem, CircleButton } from 'FillTheFridge/app/components';

import styles from './ProductsLibrary.style';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

import { addProductToList } from 'FillTheFridge/app/store/list/actions';
import { removeProduct, selectCategory, cleanSubcategories } from 'FillTheFridge/app/store/products/actions';

const ProductsLibrary = () => {
    const dispatch = useDispatch();
    const { navigate } = useNavigation();

    const { myList } = useSelector(state => state.listReducer);
    const { selectedCategory, selectedSubcategory } = useSelector(state => state.productsReducer);

    const [displaySearchInput, setDisplaySearchInput] = useState(false)
    const [searchText, setSearchText] = useState('')

    const checkIfOnList = (productTitle) => {
        let isOnLIst = false;

        myList.forEach((item, i) => {
            for (var i = 0; i < item.products.length; i++) {
                if (item.products[i].title === productTitle) {
                    isOnLIst = true;
                }
            }
        });

        return isOnLIst;
    };

    const addToList = (subcategory, product) => {
        dispatch(addProductToList(subcategory, product));
    };

    const removeItem = (subcategory, product) => {
        Alert.alert(
            product.title,
            "Supprimer ce produit ?",
            [
                {
                    text: "Annuler",
                    style: "default"
                },
                {
                    text: "Supprimer",
                    style: 'destructive',
                    onPress: () => dispatch(removeProduct(subcategory, product))
                }
            ],
            { cancelable: false }
        );
    };

    const renderHeader = () => (
        <Image
            style={styles.headerImage}
            source={{ uri: selectedCategory.image }}
        />
    );

    const renderItem = ({ item, index }) => {
        return (
            <View style={selectedSubcategory.length === index + 1 ? {marginBottom: RFValue(80)} : {}}>

                <SubcategoryItem title={item.title}/>

                {item.products.map((product, key) => (
                    <ProductItem
                        key={key}
                        product={product}
                        onPress={() => addToList(item.title, product)}
                        onLongPress={() => removeItem(item.title, product)}
                        onList={checkIfOnList(product.title)}
                    />
                ))}

            </View>
        )
    };

    useEffect(() => {
        dispatch(selectCategory({ selectedCategory: selectedCategory, searchText }))
    }, [searchText])

    return (
        <KeyboardAvoidingView
          behavior="height"
          keyboardVerticalOffset={RFValue(75)}
          style={styles.container}
        >

            <FlatList
                data={selectedSubcategory}
                ListHeaderComponent={renderHeader}
                renderItem={renderItem}
                keyExtractor={(item) => item.id.toString()}
                showsVerticalScrollIndicator={false}
            />

            {displaySearchInput &&
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        value={searchText}
                        placeholder={'Rechercher un produit'}
                        onChangeText={(text) => setSearchText(text)}
                    />
                    <CircleButton
                      icon='cross'
                      buttonStyle={{ backgroundColor: colors.danger }}
                      onPress={() => {
                          setSearchText('')
                          setDisplaySearchInput(false)
                      }}
                    />
                </View>
            }
            {!displaySearchInput &&
                <View style={styles.footerContainer}>
                    <CircleButton
                      icon='back'
                      onPress={() => dispatch(cleanSubcategories())}
                    />
                    <CircleButton
                      icon='addProduct'
                      buttonStyle={{ backgroundColor: colors.tertiary }}
                      onPress={() => navigate('AddProduct')}
                    />
                    <CircleButton
                      icon='search'
                      onPress={() => setDisplaySearchInput(true)}
                    />
                </View>
            }

        </KeyboardAvoidingView>
    )
};

export default ProductsLibrary;
