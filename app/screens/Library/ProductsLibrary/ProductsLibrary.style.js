import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.background,
    },

    headerImage: {
        resizeMode: 'cover',
        height: RFValue(80),
    },

    inputContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        padding: RFValue(10),
        backgroundColor: colors.primary,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    input: {
        width: RFValue(250),
        height: RFValue(40),
        paddingHorizontal: RFValue(10),
        backgroundColor: colors.white,
        borderRadius: RFValue(5),
        borderWidth: 1,
        borderColor: colors.black,
        color: colors.black,
    },

    footerContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        paddingHorizontal: RFValue(10),
        paddingVertical: RFValue(10),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

});
