import { StyleSheet } from 'react-native';
import { colors } from 'FillTheFridge/app/constants';
import { RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.background,
    },

    scrollView: {
        paddingTop: 15,
        paddingHorizontal: RFValue(10),
    },

    loading: {
        marginTop: RFValue(300),
    },

});
