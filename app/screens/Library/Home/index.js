import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';

import { View, ScrollView, Text, ActivityIndicator } from 'react-native';
import { CategoryItem } from 'FillTheFridge/app/components';
import ProductsLibrary from '../ProductsLibrary';

import styles from './Library.style';
import { colors } from 'FillTheFridge/app/constants';

import { fetchSubcategories } from 'FillTheFridge/app/api/subcategories';
import { fetchProducts, selectCategory } from 'FillTheFridge/app/store/products/actions';

const Library = () => {
    const dispatch = useDispatch();

    const { categories, selectedCategory } = useSelector(state => state.productsReducer);
   
    const fetch = async () => {
        const data = await fetchSubcategories();
        dispatch(fetchProducts({data: data}));
    };

    useEffect(() => {
        fetch();
    }, []);

    if (categories.length === 0) {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.tertiary} style={styles.loading}/>
            </View>
        );
    }

    return (
        <View style={styles.container}>

            {!selectedCategory && <ScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>

                {categories.map((category, key) => (
                    <CategoryItem
                        key={key}
                        style={key + 1 === categories.length ? {marginBottom: 30} : {}}
                        title={category.title}
                        image={category.image}
                        onPress={() => dispatch(selectCategory({selectedCategory: category, searchText: ''}))}
                    />
                ))}

            </ScrollView>}

            {selectedCategory &&
                <ProductsLibrary/>
            }

        </View>
    );
};

export default Library;
