import {
    ADD_PRODUCT_TO_LIST,
    REMOVE_PRODUCT_TO_LIST,
    CLEAR_MY_LIST,
    VALIDATE_LIST,
    CHECK_PRODUCT,
    UNCHECK_PRODUCT,
    UNCHECK_ALL_PRODUCTS,
} from './types';

const initialState = {
    myList: [],
    validatedList: false,
    checkList: [],
};

const listReducer = (state = initialState, action) => {
    let newList = [...state.myList];
    let newCheckList = [...state.checkList];

    switch (action.type) {
        case ADD_PRODUCT_TO_LIST:
            var index = newList.findIndex((subcategory) => subcategory.subcategory === action.subcategory);

            if (index === -1) {
                newList.push({subcategory: action.subcategory, products: [action.product]});
            } else {
                for (var i = 0; i < newList.length; i++) {
                    if (action.subcategory === newList[i].subcategory) {
                        newList[i].products = [
                            action.product,
                            ...newList[i].products.filter((product) => product.title !== action.product.title),
                        ];
                    }
                }
            };

            return {
                ...state,
                myList: newList,
            };

        case REMOVE_PRODUCT_TO_LIST:
            for (var i = 0; i < newList.length; i++) {
                if (newList[i].subcategory === action.subcategory) {
                    if (newList[i].products.length === 1) {
                        const filteredList = newList.filter((obj) => obj.subcategory !== action.subcategory);
                        return {
                            ...state,
                            myList: filteredList,
                        };
                    } else {
                        const filteredProducts = newList[i].products.filter((product) => product.title !== action.product.title);
                        newList[i].products = filteredProducts;
                        return {
                            ...state,
                            myList: newList,
                        };
                    }
                }
            };

        case CLEAR_MY_LIST:
            return {
                ...state,
                myList: [],
                checkList: [],
            };

        case VALIDATE_LIST:
            return {
                ...state,
                validatedList: action.boolean,
            };

        case CHECK_PRODUCT:
            newCheckList.push(action.product);

            return {
                ...state,
                checkList: newCheckList,
            };

        case UNCHECK_PRODUCT:
            const filteredCheckList = newCheckList.filter((product) => product.title !== action.product.title);

            return {
                ...state,
                checkList: filteredCheckList,
            };

        case UNCHECK_ALL_PRODUCTS:
            return {
                ...state,
                checkList: [],
            }

        default:
            return state;
    }
};

export default listReducer;
