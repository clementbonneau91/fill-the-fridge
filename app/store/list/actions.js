import {
    ADD_PRODUCT_TO_LIST,
    REMOVE_PRODUCT_TO_LIST,
    CLEAR_MY_LIST,
    VALIDATE_LIST,
    CHECK_PRODUCT,
    UNCHECK_PRODUCT,
    UNCHECK_ALL_PRODUCTS,
} from './types';

export const addProductToList = (subcategory, product) => (dispatch) => {
    dispatch({
        type: ADD_PRODUCT_TO_LIST,
        subcategory,
        product,
    });
};

export const removeProductToList = (subcategory, product) => (dispatch) => {
    dispatch({
        type: REMOVE_PRODUCT_TO_LIST,
        subcategory,
        product,
    })
};

export const clearMyList = () => {
    return {
        type: CLEAR_MY_LIST
    }
};

export const validateList = (boolean) => (dispatch) => {
    dispatch({
        type: VALIDATE_LIST,
        boolean,
    })
};

export const checkProduct = (product) => (dispatch) => {
    dispatch({
        type: CHECK_PRODUCT,
        product,
    })
};

export const uncheckProduct = (product) => (dispatch) => {
    dispatch({
        type: UNCHECK_PRODUCT,
        product,
    })
};

export const uncheckAllProducts = () => {
    return {
        type: UNCHECK_ALL_PRODUCTS
    }
};
