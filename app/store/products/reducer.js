import {
    FETCH_PRODUCTS,
    ADD_NEW_PRODUCT,
    REMOVE_PRODUCT,
    SELECT_CATEGORY,
    CLEAN_SUBCATEGORIES,
} from './types';

const initialState = {
    categories: [],
    subcategories: [],
    selectedCategory: null,
    selectedSubcategory: [],
};

const productsReducer = (state = initialState, action) => {
    let newCategories = [...state.categories];
    let newSubCategories = [...state.subcategories];

    switch (action.type) {
        case FETCH_PRODUCTS:
            action.data.forEach((subcategory) => {
                newCategories = [
                    subcategory.category,
                    ...newCategories.filter((item) => item.title !== subcategory.category.title),
                ];

                let alreadyExist = false;

                newSubCategories.forEach((obj) => {
                    if (subcategory.id === obj.id) {
                        alreadyExist = true;
                    }
                });

                if (!alreadyExist) {
                    newSubCategories = [
                        subcategory,
                        ...newSubCategories.filter((item) => item.id !== subcategory.id),
                    ];
                }
            });

            newCategories.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
            newSubCategories.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));

            newSubCategories.forEach((subCat) => {
                subCat.products.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
            });


            state = {
                ...state,
                categories: newCategories,
                subcategories: newSubCategories,
            };

            break;

        case ADD_NEW_PRODUCT:
            newSubCategories.forEach((subcategory, i) => {
                if (subcategory.id === action.product.subcategoryId) {
                    subcategory.products = [
                        { title: action.product.title },
                        ...subcategory.products.filter((product) => product.title !== action.product.title),
                    ];

                    subcategory.products.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
                };
            });

            state = {
                ...state,
                subcategories: newSubCategories,
            };

            break;

        case REMOVE_PRODUCT:
            newSubCategories.forEach((subcategory, i) => {
                if (subcategory.title === action.subcategory) {
                    subcategory.products = [
                        ...subcategory.products.filter((product) => product.title !== action.product.title),
                    ];
                };
            });

            state = {
                ...state,
                subcategories: newSubCategories,
            };

            break;

        case SELECT_CATEGORY:
            let newTab = newSubCategories.filter(subcategory => subcategory.category_id === action.selectedCategory.id);
            
            if (action.searchText.length === 0) {
                state = {
                    ...state,
                    selectedCategory: action.selectedCategory,
                    selectedSubcategory: newTab
                }
            } else {
                let newFilteredTab = [];

                const regex = new RegExp(action.searchText.toLowerCase());

                newTab.forEach((subcategory) => {
                    let filteredProductsTab = [];
                    for (let i = 0; i < subcategory.products.length; i++) {
                        if (regex.test(subcategory.products[i].title.toLowerCase())) {
                            filteredProductsTab.push(subcategory.products[i]);
                        }
                    }

                    if (filteredProductsTab.length > 0) {
                        newFilteredTab.push({
                            id: subcategory.id,
                            category_id: subcategory.category_id,
                            title: subcategory.title,
                            products: filteredProductsTab
                        })
                    }
                });

                state = {
                    ...state,
                    selectedCategory: action.selectedCategory,
                    selectedSubcategory: newFilteredTab
                }
            }

            break;

        case CLEAN_SUBCATEGORIES:
            state = {
                ...state,
                selectedCategory: null,
                selectedSubcategory: []
            }

            break;

        default:
            state = { ...state };
            break;
    }

    return state;
};

export default productsReducer;
