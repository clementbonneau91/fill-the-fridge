import {
    FETCH_PRODUCTS,
    ADD_NEW_PRODUCT,
    REMOVE_PRODUCT,
    SELECT_CATEGORY,
    CLEAN_SUBCATEGORIES,
} from './types';

export const fetchProducts = ({ data }) => {
    return {
        type: FETCH_PRODUCTS,
        data,
    }
};

export const addNewProduct = (product) => {
    return {
        type: ADD_NEW_PRODUCT,
        product,
    }
};

export const removeProduct = (subcategory, product) => {
    return {
        type: REMOVE_PRODUCT,
        subcategory,
        product,
    }
};

export const selectCategory = ({ selectedCategory, searchText }) => {
    return {
        type: SELECT_CATEGORY,
        selectedCategory,
        searchText
    }
}

export const cleanSubcategories = () => {
    return {
        type: CLEAN_SUBCATEGORIES
    }
}